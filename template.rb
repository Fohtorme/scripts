# encoding: windows-1252
#require 'template.rb'

# Template class
class Template
  # Script constructor
  def initialize()
    $mensagem = "Hey stranger, help me escape from this prison!!!"
  end

  # Begin of script
  def run(args)
    puts "Arguments: #{args}"
    puts "#{$mensagem}"
  end

end

Template.new().run(ARGV)
