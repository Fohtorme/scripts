# encoding: windows-1252
require 'sqlite3'

# Template class
class DataBase
  # Script constructor
  def initialize()
  end

  # Begin of script
  def run(args)
    db = SQLite3::Database.new('C:\DataBase\test.db')

    result = db.execute "select count(*) from sqlite_master where type='table' and name='users';"
    if result[0][0] == 0
      rows = db.execute <<-SQL
                create table users(
                 id int,
                 name varchar(30)
               );
              SQL
    end
    db.execute("INSERT INTO users(id, name) VALUES(?, ?)", [1, 'Bugs Bunny'])
    db.execute("select * from users where name='Bugs Bunny'") do |result|
      puts result
    end
  end

end

DataBase.new().run(ARGV)
