@echo off
SETLOCAL

REM Define Parameters
:Parameters
  REM If the user needs help
  set help=FALSE
  if "%1" == "help"  set help=TRUE
  if "%1" == "/h"    set help=TRUE
  if "%1" == "/?"    set help=TRUE
  if "%1" == "-help" set help=TRUE
  if "%1" == "/help" set help=TRUE
  if %help% == TRUE (
    goto :Help
  )
  REM If ruby is not defined, define it as true
  if not defined ruby (
    set ruby=TRUE
  )
  REM If run a ruby script
  if %ruby% == TRUE (
    goto :RubyBegin
  )
  REM Find next Parameter
  shift
  if NOT "%1" == "" (
    goto :Parameters
  )
goto :Begin

REM Start script
:Begin
  echo Fim
goto :End

REM Call ruby script
:RubyBegin
  REM TODO: I need create a bat who knows my projects and scripts directories and use then here
  call ruby C:\Scripts\agenda.rb %*
goto :End

REM Show help
:Help
  echo. Agenda functions
  echo.     insert [date] [event_description]
  echo.     list
goto :End

REM Finalize script
:End
ENDLOCAL
