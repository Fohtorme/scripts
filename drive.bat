@echo off
SETLOCAL

REM Define Parameters
:Parameters
  REM If the user needs help
  set help=FALSE
  if "%1" == "help"  set help=TRUE
  if "%1" == "/h"    set help=TRUE
  if "%1" == "/?"    set help=TRUE
  if "%1" == "-help" set help=TRUE
  if "%1" == "/help" set help=TRUE
  if %help% == TRUE (
    goto :Help
  )
  REM If other Parameter
  set drive=%1
  REM Find next Parameter
  shift
  if NOT "%1" == "" (
    goto :Parameters
  )
goto :Begin

REM Start script
:Begin
  REM Go to drive web page
  call :Drive
goto :End

REM Drive map
:Drive
  if "%drive%" == "" (
   start chrome.exe "https://drive.google.com/drive/my-drive"
   goto :eof
  )
  if /I %drive% == projects (
   start chrome.exe "https://drive.google.com/drive/folders/1UiVWO7o7Jq2QCklYRPPdhtiQq51f22uk"
   goto :eof
  )
  if /I %drive% == important (
   start chrome.exe "https://drive.google.com/drive/folders/13mwrPWTXzgH1iPWoiFiOGYCu7eNmqBq8"
   goto :eof
  )
goto :eof

REM Show help
:Help
  echo. Just tell me what drive page you want open
  echo. OBS: If you don't tell me, I will open the main page :)
  echo.     projects  - Open projects drive page
  echo.     important - Open drive page of importants things, like you S2
goto :End

REM Finalize script
:End
ENDLOCAL
