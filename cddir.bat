@echo off
SETLOCAL

REM Define Parameters
:Parameters
  REM If the user needs help
  set help=FALSE
  if /I "%1" == "help"  set help=TRUE
  if /I "%1" == "/h"    set help=TRUE
  if /I "%1" == "/?"    set help=TRUE
  if /I "%1" == "-help" set help=TRUE
  if /I "%1" == "/help" set help=TRUE
  if %help% == TRUE (
    goto :Help
  )
  REM Set Parameter as a directory
  set dirarq=%1
  REM Find next Parameter
  shift
  if NOT "%1" == "" (
    goto :Parameters
  )
goto :Begin

REM Start script
:Begin
  if /I "%dirarq%" == "/p" set dirarq=C:\Projects
  if /I "%dirarq%" == "/s" set dirarq=C:\Scripts
  call getdir.bat %dirarq%
goto :End

REM Show help
:Help
  echo "Sorry, but I can't help you because I am just a template :/"
goto :End

REM Finalize script
:End
ENDLOCAL & cd %directory%%file%
