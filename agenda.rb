# encoding: windows-1252
require 'sqlite3'

# Template class
class Agenda
  # Script constructor
  def initialize()
    # Initialize data base
    $table_event = "EVENT"
    $db = SQLite3::Database.new('C:\DataBase\event.db')
    $db.results_as_hash = true
    # If data base don't created yet
    result = $db.execute "select count(*) from sqlite_master where type='table' and name='#{$table_event}';"
    if result[0][0] == 0
        # TODO: Create a event type table too (Meeting, party, "soccer"(create event for physical exercises)
        rows = $db.execute <<-SQL
                  create table #{$table_event}(
                    id int,
                    event_date date,
                    event_description varchar(60)
                  );
                SQL
    end
  end

  # Begin of script
  def run(args)
    # Evaluate de function
    case args[0]
    when "insert"
      insert(args[1],args[2])
    when "list"
      list()
    when "date"
      puts format_date(args[1])
    end
  end

  # Get the next ID for the event inclusion
  def get_next_id()
    max_id = 0
    # TODO: Optimize this
    $db.execute("select * from '#{$table_event}'") do |result|
      if result['id'] > max_id
        max_id = result['id']
      end
    end
    return max_id + 1
  end

  # Insert events in data base
  def insert(date, description)
    if date == nil || description == nil
      return nil;
    end
    date = format_date(date)
    if date == nil
      return nil;
    end
    $db.execute("insert into '#{$table_event}' "+
                  "(id, event_date, event_description) "+
                  "values(?, ?, ?)", [get_next_id(), date, description])
  end

  # Format date to write in data base
  def format_date(date)
    if date == nil
      return nil;
    end
    regex = /([0-9]+)(\/|\-)([0-9]+)(\/|\-)([0-9]+)/
    dt = regex.match(date)
    if dt == nil
      puts "Date with invalid format!"
      return nil
    end
    return '%04i-%02i-%02i' % [dt[5].to_i, dt[3].to_i, dt[1].to_i]
  end

  # List all events from data base
  def list()
    puts "Date-------Description-------------------------------------------------"
    $db.execute("select * from '#{$table_event}' order by event_date asc") do |result|
      puts '%10s %-.60s' % [result['event_date'], result['event_description']]
    end
  end
end

Agenda.new().run(ARGV)
