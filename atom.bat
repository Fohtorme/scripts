@echo off
SETLOCAL

REM Define Parameters
:Parameters
  REM If the user needs help
  set help=FALSE
  if /I "%1" == "help"  set help=TRUE
  if /I "%1" == "/h"    set help=TRUE
  if /I "%1" == "/?"    set help=TRUE
  if /I "%1" == "-help" set help=TRUE
  if /I "%1" == "/help" set help=TRUE
  if %help% == TRUE (
    goto :Help
  )
  REM If called projects directory
  if /I "%1" == "/p" (
    call :AtomOpen C:\Projects
  REM If called scripts directory
  ) else if /I "%1" == "/s" (
    call :AtomOpen C:\Scripts
  REM If called another file or directory
  ) else (
    call :AtomOpen %1
  )
  REM Find next Parameter
  shift
  if NOT "%1" == "" (
    goto :Parameters
  )
goto :Begin

REM Start script
:Begin
  REM echo Thanks for use me :D
goto :End

:AtomOpen
  call getdir.bat %1
  echo C:\Users\jonas\AppData\Local\atom\atom.exe %directory%%file%
  call C:\Users\jonas\AppData\Local\atom\atom.exe %directory%%file%
goto :eof

REM Show help
:Help
  echo. Just tell me what file or folder you want open
  echo.     projects  - Open projects folder
  echo.     scripts   - Open scripts folder
  echo.     [file]    - Open file
  echo.     [folder]  - Open folder
goto :End

REM Finalize script
:End
ENDLOCAL
