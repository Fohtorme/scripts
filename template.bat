@echo off
SETLOCAL

REM Define Parameters
:Parameters
  REM That is the Parameter, I through then until find all :)
  echo %1
  REM If the user needs help
  set help=FALSE
  if /I "%1" == "help"  set help=TRUE
  if /I "%1" == "/h"    set help=TRUE
  if /I "%1" == "/?"    set help=TRUE
  if /I "%1" == "-help" set help=TRUE
  if /I "%1" == "/help" set help=TRUE
  if %help% == TRUE (
    goto :Help
  )
  REM Find next Parameter
  shift
  if NOT "%1" == "" (
    goto :Parameters
  )
goto :Begin

REM Start script
:Begin
  echo Fim
goto :End

REM Show help
:Help
  echo "Sorry, but I can't help you because I am just a template :/"
goto :End

REM Finalize script
:End
ENDLOCAL
